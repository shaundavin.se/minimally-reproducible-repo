import db from './app/models'

async function createDbs() {
  await db.models.Location.sync({ force: true })
  await db.models.Vendor.sync({ force: true })
  await db.models.Shipment.sync({ force: true })
}

// const promises = Object.keys(db.models).map(modelName => {
//   return db.models[modelName].sync({ force: true })
// })

async function seed () {

  await db.models.Location.create({
    name: 'Sample Origin'
  })

  await db.models.Location.create({
    name: 'Sample Destination'
  })

  await db.models.Vendor.create({
    name: 'Vendor 1'
  })

  await db.models.Vendor.create({
    name: 'Vendor 2'
  })

  await db.models.Vendor.create({
    name: 'Vendor 3'
  })

  await db.models.Vendor.create({
    name: 'Vendor 4'
  })

  await db.models.Vendor.create({
    name: 'Vendor 5'
  })

  const shipment = await db.models.Shipment.create({
    etd: new Date(2023, 5, 13),
    eta: new Date(2023, 5, 15),
    type: 'FCL',
    originId: 1,
    destinationId: 2,
    transportDocumentNumber: 'S390394HU32U',
    description: 'Some description',
    carrierId: 1,
    carrierBookingRef: '3939HUE0O',
    shipperId: 2,
    consigneeId: 3,
  })
}


createDbs().then(seed).then(() => console.log('SUCCESS'))
