import process from 'process'
// @ts-ignore
import * as pg from 'pg';

import dotenv from 'dotenv'

dotenv.config()

import {DataTypes, Sequelize} from "sequelize";


const databaseURL = <string>process.env.DATABASE_URL


export const sequelize = new Sequelize(databaseURL);

// Model Definitions -----------------------------------------------------------

export const Location = sequelize.define('Location', {
  name: {type: DataTypes.STRING, allowNull: false},
})

export const Vendor = sequelize.define('Vendor', {
  name: DataTypes.STRING,
})

export const Shipment = sequelize.define('Shipment', {
  etd: DataTypes.DATEONLY,
  eta: DataTypes.DATEONLY,
  type: DataTypes.STRING,
  originId: {type: DataTypes.INTEGER.UNSIGNED},
  destinationId: {type: DataTypes.INTEGER.UNSIGNED},
  transportDocumentNumber: DataTypes.STRING,
  description: DataTypes.TEXT,
  carrierId: DataTypes.INTEGER.UNSIGNED,
  carrierBookingRef: DataTypes.STRING,
  shipperId: DataTypes.INTEGER.UNSIGNED,
  consigneeId: DataTypes.INTEGER.UNSIGNED,
}, {
  // Other model options go here
});

// Associations  ---------------------------------------------------------------

Shipment.belongsTo(Location, {
  foreignKey: 'originId',
  as: 'Origin'
})

Location.hasMany(Shipment, {
  foreignKey: 'originId',
})

Shipment.belongsTo(Location, {
  foreignKey: 'destinationId',
  as: 'Destination'
})

Location.hasMany(Shipment, {
  foreignKey: 'destinationId',
})

Shipment.belongsTo(Vendor, {
  foreignKey: 'carrierId',
  as: 'Carrier'
})

Vendor.hasMany(Shipment, {
  foreignKey: 'carrierId',
})

Shipment.belongsTo(Vendor, {
  foreignKey: 'shipperId',
  as: 'Shipper'
})

Vendor.hasMany(Shipment, {
  foreignKey: 'shipperId',
})

Shipment.belongsTo(Vendor, {
  foreignKey: 'consigneeId',
  as: 'Consignee'
})

Vendor.hasMany(Shipment, {
  foreignKey: 'consigneeId',
})


// Set-up  ---------------------------------------------------------------------

const db: { [key: string]: any } = {
  models: {
    Shipment,
    Location,
    Vendor,
    // SeaImport,
    // SeaExport,
    // AirImport,
    // AirExport,
  }
};

db.sequelize = sequelize;
db.Sequelize = Sequelize;

export default db
