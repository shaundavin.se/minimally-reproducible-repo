import {NextResponse} from "next/server";
import {Shipment} from "@/app/models";


export async function GET() {
  const shipments = await Shipment.findAll()
  return NextResponse.json({data: JSON.stringify(shipments)}, )
}

